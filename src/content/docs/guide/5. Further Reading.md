---
title: Further Reading
description: Guide on further reading sources.
---

- [Developer README](https://gitlab.com/nfdi4culture/ta5-knowledge-graph/annotation-service/-/tree/main/readme)
- [License](https://gitlab.com/nfdi4culture/ta5-knowledge-graph/annotation-service/-/blob/main/LICENSE)
- [High-level Roadmap](https://docs.google.com/spreadsheets/d/1aCI6LHKs70q2vynO-L1MyRJqbS7IpzHY1dtj4lMIkqY/edit?gid=1007010121#gid=1007010121)
- [NFDI4Culture](https://nfdi4culture.de/)
- [TIB – Leibniz Information Centre for Science and Technology](https://www.tib.eu/en/)